# Toulouse
**A Fun and Easy-to-Use Artist Portfolio Platform**

> Henri Marie Raymond de Toulouse-Lautrec-Monfa (24 November 1864 – 9 September 1901) was a French painter, printmaker, draughtsman, caricaturist, and illustrator whose immersion in the colourful and theatrical life of Paris in the late 19th century allowed him to produce a collection of enticing, elegant, and provocative images of the modern, sometimes decadent, affairs of those times.  
>
><sup>[via Wikipedia](https://en.wikipedia.org/wiki/Henri_de_Toulouse-Lautrec)</sup>